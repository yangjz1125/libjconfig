/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.light.config;

/**
 *
 * @author yangjz
 */
public class ConfigFileErrorException extends Exception {

    /**
     * Creates a new instance of <code>ConfigFileReadException</code> without
     * detail message.
     */
    public ConfigFileErrorException() {
    }

    /**
     * Constructs an instance of <code>ConfigFileReadException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public ConfigFileErrorException(String msg) {
        super(msg);
    }
}
