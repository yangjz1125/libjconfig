/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.light.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 *
 * @author yangjz
 */
public class ConfigFile {
    File file;
    Map<String,String> configObjects;
    ConfigFile(String filename){
        file = new File(filename);
        configObjects = new HashMap<>();
    }
    
    void read() throws ConfigFileErrorException, FileNotFoundException{
        FileReader configFileReader = new FileReader(file);
        BufferedReader configFile = new BufferedReader(configFileReader);
        
        try {
            int cnt = 0;
            for(;;){
                String line = configFile.readLine();
                ++cnt;
                if(line == null || line.equals("")){
                    break;
                }
                String[] values = line.split("-,2");
                if(values[0].matches("\\s*") || values[1].matches("\\s*")){
                    throw new ConfigFileErrorException(String.format("Line %d: Any config or key can not be empty.", cnt));
                }
                configObjects.put(values[0], values[1]);
            }
            configFile.close();
        } catch (IOException ex) {
        }
    }
    
    String get(String key){
        return configObjects.get(key);
    }
}
